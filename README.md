# archlinux-go
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Build Status](https://drone.dotya.ml/api/badges/wanderer-containers/archlinux-go/status.svg)](https://drone.dotya.ml/wanderer-containers/archlinux-go)

this repo provides a Containerfile for building Go applications.
based on `docker.io/immawanderer/archlinux`, weekly rebuilt on cron.

### LICENSE
GPL-3.0
